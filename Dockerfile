FROM mongo:5.0.24-focal

# update the repository sources list
# and install dependencies

RUN apt-get update \
    && apt-get install -y --no-install-recommends openssh-server \
    && apt-get install -y --no-install-recommends openssh-client \
    && apt-get install -y --no-install-recommends sshpass \
    && apt-get install -y --no-install-recommends bash \
    && apt-get install -y --no-install-recommends apt-utils \
    && apt-get install -y --no-install-recommends apt-transport-https \
    && apt-get install -y --no-install-recommends sudo \
    && apt-get install -y --no-install-recommends curl \
    && apt-get install -y --no-install-recommends build-essential \
    && apt-get install -y --no-install-recommends libssl-dev \
    && apt-get install -y --no-install-recommends libev-dev \
    && apt-get install -y --no-install-recommends udev \
    && apt-get install -y --no-install-recommends git \
    && apt-get install -y --no-install-recommends python \
    && apt-get install -y --no-install-recommends make \
    && apt-get install -y --no-install-recommends g++ \
    && apt-get install -y --no-install-recommends wget \
    && apt-get install -y --no-install-recommends usbutils \
    && apt-get install -y --no-install-recommends nano \
    && apt-get install -y --no-install-recommends cron \
    && apt-get install -y --no-install-recommends bluetooth \
    && apt-get install -y --no-install-recommends bluez \
    && apt-get install -y --no-install-recommends libbluetooth-dev \
    && apt-get install -y --no-install-recommends libudev-dev \
    && apt-get install -y --no-install-recommends etherwake \
    && apt-get install -y --no-install-recommends libasound2  \
    && apt-get install -y --no-install-recommends libasound2-dev  \
    && apt-get install -y --no-install-recommends alsa-utils \
    && apt-get install -y --no-install-recommends libreoffice \
    && apt-get install -y --no-install-recommends default-jre \
    && apt-get install -y --no-install-recommends imagemagick \
    && apt-get install -y --no-install-recommends ghostscript  \
    && apt-get install -y --no-install-recommends graphicsmagick  \
    && apt-get install -y --no-install-recommends iputils-ping  \
    && apt-get -y autoclean


# TODO check pupetier / chromium install

# apt-get update && \
# apt-get -yq install libatk1.0-0 libgtk2.0-0 libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2 libxtst6 libasound2 xauth xvfb

#    apt-get update \
#    && apt-get install -y wget gnupg \
#    && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
#    && sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
#    && apt-get update \
#    && apt-get install -y google-chrome-stable fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-kacst fonts-freefont-ttf libxss1 \
#    --no-install-recommends \
#    && rm -rf /var/lib/apt/lists/*

#    && groupadd -r pptruser && useradd -r -g pptruser -G audio,video pptruser \
#    && mkdir -p /home/pptruser/Downloads \
#    && chown -R pptruser:pptruser /home/pptruser \
#    && chown -R pptruser:pptruser /node_modules \
#    && chown -R pptruser:pptruser /package.json \
#    && chown -R pptruser:pptruser /package-lock.json

# Run everything after as non-privileged user.
# USER pptruser

# end TODO check pupetier / chromium install

#&& echo "root:$ROOT_PW" | chpasswd
ENV ROOT_PW "Docker!"

# Copy the sshd_config file to the /etc/ssh/ directory
COPY ./src/misc/sshd_config /etc/ssh/

# Copy and configure the ssh_setup file
RUN mkdir -p /tmp
COPY ./src/misc/ssh_setup.sh /tmp
RUN chmod +x /tmp/ssh_setup.sh \
    && (sleep 1;/tmp/ssh_setup.sh 2>&1 > /dev/null)

### configure timeZone

RUN echo "Europe/Berlin" > /etc/timezone \
    && ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime \
    && dpkg-reconfigure -f noninteractive tzdata

### Install node

RUN echo "Nodejs User: node  Group: gid 1000 node"
RUN groupadd --gid 1000 node \
  && useradd --uid 1000 --gid node --shell /bin/bash --create-home node

# replace shell with bash so we can source files
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

RUN mkdir -p /usr/local/nvm

# nvm environment variables
ENV NVM_DIR /usr/local/nvm
ENV NODE_VERSION 18.18.2
ENV NODE_ENV 'production'

# install nvm
# https://github.com/creationix/nvm#install-script
# RUN curl --silent -o- https://raw.githubusercontent.com/creationix/nvm/v0.31.2/install.sh | bash
RUN curl --silent -o- https://raw.githubusercontent.com/nvm-sh/nvm/master/install.sh | bash

# install node and npm
RUN source $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION \
    && nvm alias default $NODE_VERSION \
    && nvm use default

# add node and npm to path so the commands are available
ENV NODE_PATH $NVM_DIR/versions/node/v$NODE_VERSION/lib/node_modules
ENV PATH $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH
ENV PATH /opt/X11/bin:$PATH

# confirm installation
RUN node -v
RUN npm -v
RUN npm root -g

### install yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -\
    && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list  \
    && apt-get install -y --no-install-recommends yarn

### setup npm apps

COPY ./src /run
RUN chmod +x -R /run \
&&  mkdir -p /config

ENV ADMIN_USER="admin" \
    ADMIN_PWD="changeme" \
    AUTH="y" \
    DB_NAME="" \
    DB_USER="user" \
    DB_PWD="changeme" \
    APP_DIR="serverApi" \
    TZ="Europe/Berlin" \
    USE_DEPLOYD="n"

# install ncu
RUN npm install -g npm-check-updates

# install node-red
RUN npm install -g --unsafe-perm node-red

# install feathersjs
RUN npm install -g @feathersjs/cli

# install deployd
RUN if [ "$USE_DEPLOYD" = "y" ]; then \
    npm install -g deployd-cli;  \
    fi

# install pm2
RUN npm install -g pm2

# install shx
RUN npm install -g shx

# install typescript
RUN npm install -g typescript

# install frontail
RUN npm install -g frontail

RUN mkdir -p /app

# deployd
EXPOSE 2403
# nodered
EXPOSE 1880
# feathers
EXPOSE 3030
# frontail logs
EXPOSE 9001

ENTRYPOINT ["/run/entrypoint.sh"]
