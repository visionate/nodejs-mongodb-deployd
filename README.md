# Deployd Docker Image

## build it

```bash
docker build -t wbeckervid/nodejsmongodbdeployd:test .
```

## run it

```bash

# run the Image
docker run -i -t -p 3030:3030 \
-e ROOT_PW="Docker!" \
-e ADMIN_USER="admin" \
-e ADMIN_PWD="changeme" \
-e DB_NAME="mydbname" \
-e DB_USER="user" \
-e DB_PWD="changeme" \
-e APP_DIR="server-api" \
-v $PWD/containerData/mongoData:/data/db  \
-v $PWD/containerData/app:/app  \
--name api \
wbeckervid/nodejsmongodbdeployd:test

```

## git tag

    git tag -a v1.5.4 -m 'Version v1.5.4 (Node 8.12.0, MongoDB 3.6.8)'
    git tag -a v1.5.5 -m 'Version v1.5.5 (updated node to v10. pm2 instead of forever. added build-essential, usbutils, wget, udev and nano)'
    git tag -a v1.5.7 -m 'Version v1.5.7 (added frontail)'
    git tag -a v1.5.10 -m 'Version v1.5.10 (tune entrypoint, add crontab, add NODE_ENV)'
    git tag -a v1.5.11 -m 'Version v1.5.11 (removed startscript)'
    git tag -a v1.6.0 -m 'Version v1.6.0 (root to /app/serverApi)'
    git tag -a v1.6.1 -m 'Version v1.6.1 (root to /app/serverApi)'
    git tag -a v1.6.2 -m 'Version v1.6.2 (added bluetooth)'
    git tag -a v1.6.3 -m 'Version v1.6.32 (add ENV /opt/X11/bin)'
    git tag -a v1.6.4 -m 'Version v1.6.4 (add etherwake, set timezone to Berlin)'
    git tag -a v1.6.5 -m 'Version v1.6.5 (add etherwake, set timezone to Berlin)'
    git tag -a v1.6.6 -m 'Version v1.6.6 (add yarn)'
    git tag -a v1.6.7 -m 'Version v1.6.7 (fix yarn)'
    git tag -a v1.6.8 -m 'Version v1.6.8 (add openssh-client)'
    git tag -a v1.6.9 -m 'Version v1.6.9 (fix set timezone to Berlin)'
    git tag -a v1.6.10 -m 'Version v1.6.10 (fix set timezone to Berlin)'
    git tag -a v1.7.0 -m 'Version v1.7.0 (fix docker file + mongo:3.6.11)'
    git tag -a v1.7.1 -m 'Version v1.7.1 (add --bind_ip_all all to mongodb)'
    git tag -a v1.8.0 -m 'Version v1.8.0 (updated nodejs to v12 )'
    git tag -a v1.8.1 -m 'Version v1.8.1 (trigger auto build )'
    git tag -a v2.0.0 -m 'Version v2.0.0 ( add libreoffice, imagemagick,  ghostscript, graphicsmagick )'
    git tag -a v2.1.0 -m 'Version v2.1.0 ( NODE_VERSION 14.18.2, mongo:4.2-rc )'
    git tag -a v2.1.1 -m 'Version v2.1.1 ( enable auth )'
    git tag -a v2.1.2 -m 'Version v2.1.2 ( add ssh-server )'
    git tag -a v2.1.3 -m 'Version v2.1.3 ( changed /app/serverApi" to /app/$APP_DIR, removed deployd, added feathers CLI)'
    git tag -a v2.1.4 -m 'Version v2.1.4 ( NODE_VERSION from 14.18.2 to 16.15.0, mongo from 4.2-rc to 4.4.14-focal )'
    git tag -a v2.1.5 -m 'Version v2.1.5 ( NODE_VERSION from 16.15.0 to 16.20.2 )'
    git tag -a v2.1.6 -m 'Version v2.1.6 ( removed postfix && ssmtp )'
    git tag -a v2.2.0 -m 'Version v2.2.0 ( NODE_VERSION from 16.20.2 to 18.18.2 mongo from 4.4.14 to 5.0.24 )'
    git tag -a v2.2.1 -m 'Version v2.2.1 ( change mongo shell to mongosh )'
    git tag -a v2.2.2 -m 'Version v2.2.2 ( fix auth )'
    git tag -a v2.2.3 -m 'Version v2.2.3 ( fix CRLF to LF )'
    git tag -a v2.2.4 -m 'Version v2.2.4 ( start.sh )'
    git tag -a v2.2.5 -m 'Version v2.2.5 ( fix enable auth )'
    git tag -a v2.2.6 -m 'Version v2.2.6 ( Add optional deployd installation to Dockerfile )'

## Docker Hub

[nodejsmongodbdeployd](https://hub.docker.com/r/wbeckervid/nodejsmongodbdeployd/)

```bash

# build && push image
docker build -t wbeckervid/nodejsmongodbdeployd:v2.2.6 .
docker login
docker images
docker tag f62fa8139705 wbeckervid/nodejsmongodbdeployd:v2.2.6
docker push docker.io/wbeckervid/nodejsmongodbdeployd:v2.2.6

```
