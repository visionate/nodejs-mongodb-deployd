#!/bin/bash
RET=1

mongoshell=$(/run/misc/mongoshell.sh)

while [[ RET -ne 0 ]]; do
    echo "=> Waiting for confirmation of MongoDB service startup"
    # echo "$mongoshell" admin --eval "help" >/dev/null 2>&1
    $mongoshell admin --eval "help" >/dev/null 2>&1
    RET=$?
    sleep 10
done
