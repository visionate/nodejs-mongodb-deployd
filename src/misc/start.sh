#!/bin/bash
cmd="mongod --port 27017 --bind_ip_all"
if [ "$AUTH" == "y" ] && [ -f /data/db/.admin_created ];then
  cmd="mongod --auth --port 27017 --bind_ip_all"
fi
echo "$cmd" "AUTH: $AUTH"
$cmd
