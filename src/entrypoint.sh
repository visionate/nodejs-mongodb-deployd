#!/bin/bash

set -m

# set root Password
echo "set root Password '$ROOT_PW'"
echo "root:$ROOT_PW" | chpasswd

# starting sshd
echo "starting sshd '/usr/sbin/sshd'"
/usr/sbin/sshd

# START in background
/run/misc/start.sh&

# wait
/run/misc/wait_until_started.sh

# CONFIGURE AUTHENTICATION
if [ ! -f /data/db/.admin_created ]; then
  echo mongosh admin --eval "db.createUser({user: '$ADMIN_USER', pwd: '$ADMIN_PWD', roles:[{role: 'userAdminAnyDatabase', db: 'admin'},{role: 'readWriteAnyDatabase', db: 'admin'}]});"
  mongosh admin --eval "db.createUser({user: '$ADMIN_USER', pwd: '$ADMIN_PWD', roles:[{role: 'userAdminAnyDatabase', db: 'admin'},{role: 'readWriteAnyDatabase', db: 'admin'}]});"
  touch /data/db/.admin_created
fi

# CREATE DB IF SPECIFIED
if [ ! -f /data/db/.db_created ] && [ "$DB_NAME" != "" ]; then
    mongoshell=$(/run/misc/mongoshell.sh)
    echo "$mongoshell" "$DB_NAME" --eval "db"
    $mongoshell "$DB_NAME" --eval "db"
    touch /data/db/.db_created
fi

# CREATE DB OWNER
if [ ! -f /data/db/.db_owner_created ]; then
    mongoshell=$(/run/misc/mongoshell.sh)
    echo "$mongoshell" "$DB_NAME" --eval "db.createUser({user: '$DB_USER', pwd: '$DB_PWD', roles:[{role: 'dbOwner', db: '$DB_NAME'}]});"
    $mongoshell "$DB_NAME" --eval "db.createUser({user: '$DB_USER', pwd: '$DB_PWD', roles:[{role: 'dbOwner', db: '$DB_NAME'}]});"
    touch /data/db/.db_owner_created
fi

# RUN npm run start
if [ -f app/$APP_DIR/package.json ]; then
  echo "cd app/$APP_DIR"
  cd app/$APP_DIR || exit
  echo "npm run start"
  npm run start
fi

# Place job in the foreground, and make it the current job using fg command
fg
